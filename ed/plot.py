import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

lat = [1, 2, 4, 6, 8]
time = [0.001, 0.006, 0.510, 113.472, 34602.560]

def fitfunc( x, p0, p1, p2):
	return p0*np.exp(p1 * x)
def errfunc(p, x, y):
	return fitfunc(p, x) - y
popt, pcov = optimize.curve_fit(fitfunc, lat, time)
print(popt)
xfit = np.linspace(1., 8., 20)

plt.figure()
#plt.plot(T, ed_v0_0, '.-', label='V=0.0')
#plt.plot(T, ed_v0_3, '.-', label='V=0.3')
plt.plot(lat, time, 'o', xfit, fitfunc(xfit, *popt), '--')
plt.xlabel('number of sites')
plt.ylabel('time (s)')
plt.title('2D square lattice')
plt.legend()
plt.show()
