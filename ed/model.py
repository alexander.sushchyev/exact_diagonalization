import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mp
import itertools as it
import more_itertools as mit
from matplotlib import colors


# remove duplicate states from list
def rm_dup(arg_list):
	result = []
	for i in arg_list:
		i = list(i)
		if i not in result:
			result.append(i)
	return result


def bit2int(states):
	ind = states.dot(1 << np.arange(states.shape[-1] - 1, -1, -1))
	return ind


class Model:
	""" This is an implementation of the Hubbard model on a square lattice.
    The class Model takes the number of lattice sites in x and y direction of a square lattice to make a unit cell with
    periodic boundary conditions. Calculate H_t(|n1n2...nN>_up x |n1n2...nN>_down) in particle number basis. The total
    number of states in this basis equals 4**N_sites. Here 2 symmetries of the Hamiltonian are used to reduce this
    number. H_U, H_V are already diagonal in this basis. Add everything up for the Hamiltonian represented in the basis
    of the particle number operator and diagonalize this matrix, i.e. get eigenvalues and eigenvectors.
    """

	def __init__(self, nx, ny, t, u, v, mu, one_dim):
		self.nx = int(nx)
		self.ny = int(ny)
		self.lat = None
		self.nsites = None
		self.nnlist = None
		self.vec_dict = None
		self.t = t
		self.u = u
		self.v = v
		self.mu = mu
		self.one_dim = one_dim

	def make_lattice(self, state=None):
		if not state:
			self.lat = np.zeros((self.nx, self.ny))
		else:
			# TODO: display state as lattice configuration: up=1, down=-1 and phase
			pass
		self.nsites = self.nx * self.ny

	def print_lattice(self):
		print(self.lat)

	def get_nnlist(self):
		""" Write the index of nearest neighbours of site i into dict; lat site: list[nn site] including periodic
            boundary conditions."""
		nx = self.nx
		ny = self.ny
		self.nnlist = {}
		if self.one_dim:
			for i in range(nx):
				self.nnlist[i + 1] = [(i + 1) % nx + 1]
		else:
			for i in range(ny):
				for j in range(i * nx, i * nx + nx):
					self.nnlist[j + 1] = [(j + 1) % nx + i * nx + 1, (j + nx) % (nx * ny) + 1]

	def get_vec_dict(self):
		""" transform a lattice sites index into a 2D vectors
		"""
		self.vec_dict = {}
		if self.one_dim:
			for i in range(self.nx):
				tmp_vec = np.zeros(2)
				tmp_vec[0] = i
				self.vec_dict[i + 1] = tmp_vec
		else:
			for i in range(self.ny):
				for j in range(self.nx):
					tmp_vec = np.zeros(2)
					tmp_vec[0] = j
					tmp_vec[1] = i
					self.vec_dict[i*self.ny + 1 + j] = tmp_vec

	def min_dist(self, x0, x1):
		""" calculate the minimal distance of two lattice sites on a torus
		:param
			x0 : 2darray - starting point
			x1 : 2darray - shifting vector end
		:return
			x_min : 2darray - minimal shift vector
		"""
		l1 = np.array([self.nx, 0])
		l2 = np.array([0, self.ny])
		n1_min = 0.
		n2_min = 0.
		x = x1 - x0
		x_norm_min = np.linalg.norm(x)
		for n1 in range(-1, 2):
			for n2 in range(-1, 2):
				x_min = x + n1*l1 + n2*l2
				x_norm = np.linalg.norm(x_min)
				if x_norm < x_norm_min:
					n1_min = n1
					n2_min = n2
					x_norm_min = x_norm
		x_min = x + n1_min*l1 + n2_min*l2
		return x_min

	def v_xy(self, x_min, alpha):
		""" Calculate the Coulomb interaction matrix elements
		:param
			x_min : 2darray - minimal distance of two lattice sites
			alpha : float - damping Coulomb repulsion
		:return
			val : float - value of the Coulomb force between site x with y
		"""
		x_norm = np.linalg.norm(x_min)
		if x_norm == 0.:
			val = self.u
		else:
			val = self.u / x_norm * alpha
		return val

	def states(self, n):
		""" Create the particle number basis of the whole Hilbert space
		:param
			n : int - number of electrons
		:return
			states : ndarray - state vectors corresponding to the particle number n
        """
		n_error = 'number of electrons m has to be smaller or equal to 2*n (double the number of lattice sites)'
		assert 0 <= n <= 2 * self.nsites, n_error
		m = 2 * self.nsites  # maximal number of particles

		sign = np.ones(1, dtype=float)
		init_occ = np.ones(n, dtype=int)
		init_unocc = np.zeros(m - n, dtype=int)
		init_state = np.append(init_occ, init_unocc)
		states_perm = list(mit.distinct_permutations(init_state))
		states = np.array([np.append(sign, i) for i in states_perm])
		return states

	def states_N_S0(self, mup=None, mdown=None):
		""" Create (further reduced) particle number basis including particle number conservation and Sz symmetry.
            We know that the ground state is a singlet state, i.e. spins alternate on the sites.
            Take only even numbers of lattice sites.
            Number of states = product over spins of (N_sites choose N_electrons_spin) (2 binomial coefficients)
        		:param
        			mup : int - number of electrons with spin up, default is Nsites/2
        			mdown : int - number of electrons with spin down, default is Nsites/2
        		:return
        			states : ndarray - all state vectors
        """
		assert self.nsites % 2 == 0
		if mup is None and mdown is None:
			mup = int(self.nsites / 2)
			mdown = int(self.nsites / 2)
		m = mup + mdown
		m_error = 'number of electrons m has to be smaller or equal to 2*n (double the number of lattice sites)'
		assert 0 <= m <= 2 * self.nsites, m_error

		sign = np.ones(1, dtype=int)
		init_up = np.append(np.zeros(self.nsites - mup, dtype=int), np.ones(mup, dtype=int))
		init_down = np.append(np.zeros(self.nsites - mdown, dtype=int), np.ones(mdown, dtype=int))
		statesup = list(mit.distinct_permutations(init_up))
		statesdown = list(mit.distinct_permutations(init_down))
		states = np.array([np.append(sign, [i, j]) for i, j in list(it.product(statesup, statesdown))])
		return states

	# TODO: individual index for each state
	""" this doesn't work (yet)
    def index(self, i, j):
        return i * 2**self.nsites + j
    """

	# Action of the field operators on particle number states; first entry, i.e. state[0] = phase factor (+1 or -1)
	# fermionic creation operator
	def c_dag(self, state, ind, spin):
		if not state.any():
			return np.array([0])
		assert 1 <= ind <= self.nsites
		if spin == 'up':
			state[ind] += 1
			if 0 <= state[ind] < 2:
				phase = 0
				for i in state[1:ind]:
					phase += i
				if phase % 2 == 0:
					state[0] *= 1
				elif phase % 2 == 1:
					state[0] *= -1
				return state
			else:
				return np.array([0])
		if spin == 'down':
			state[self.nsites + ind] += 1
			if 0 <= state[self.nsites + ind] < 2:
				phase = 0
				for i in state[self.nsites + 1:self.nsites + ind]:
					phase += i
				if phase % 2 == 0:
					state[0] *= 1
				elif phase % 2 == 1:
					state[0] *= -1
				return state
			else:
				return np.array([0])

	# fermionic annihilation operator
	def c(self, state, ind, spin):
		if not state.any():
			return np.array([0])
		assert 1 <= ind <= self.nsites
		if spin == 'up':
			state[ind] -= 1
			if 0 <= state[ind] < 2:
				phase = 0
				for i in state[1:ind]:
					phase += i
				if phase % 2 == 0:
					state[0] *= 1
				elif phase % 2 == 1:
					state[0] *= -1
				return state
			else:
				return np.array([0])
		if spin == 'down':
			state[self.nsites + ind] -= 1
			if 0 <= state[self.nsites + ind] < 2:
				phase = 0
				for i in state[self.nsites + 1:self.nsites + ind]:
					phase += i
				if phase % 2 == 0:
					state[0] *= 1
				elif phase % 2 == 1:
					state[0] *= -1
				return state
			else:
				return np.array([0])

	def hamt(self, state):
		""" Action of the kinetic part of the hamiltonian on the states
		:param
			state: 1darray - particle number state generated by states()
		:return
		    tstates: 2darray - array containing lists that store the action of H_t corresponding to each eigenstate
        """
		tstates = []  # store action of H_t on states
		nn = self.nnlist
		if self.one_dim:
			dim = 1
		else:
			dim = 2
		for i in nn:  # iterate over lattice sites, i.e. keys of nnlist
			for j in range(dim):  # number of dimension of lattice = number of nn per site / 2 (one way counting)
				# action of the field operators on a state
				# c_i^{dagger}c_j spin up
				tmp = self.c_dag(self.c(np.copy(state), nn[i][j], 'up'), i, 'up')
				if tmp.any():
					tstates.append(tmp)
				# c_j^{dagger}c_i spin up
				tmp = self.c_dag(self.c(np.copy(state), i, 'up'), nn[i][j], 'up')
				if tmp.any():
					tstates.append(tmp)
				# c_i^{dagger}c_j spin down
				tmp = self.c_dag(self.c(np.copy(state), nn[i][j], 'down'), i, 'down')
				if tmp.any():
					tstates.append(tmp)
				# c_j^{dagger}c_i spin down
				tmp = self.c_dag(self.c(np.copy(state), i, 'down'), nn[i][j], 'down')
				if tmp.any():
					tstates.append(tmp)
		if self.one_dim:
			tstates = np.array(tstates)
		else:
			tstates = np.array(tstates)
		return tstates

	def matrix(self, bra, ket, t, u, v, mu):
		"""Hamiltonian matrix represented in particle number basis; H:C^{dim1 x dim2}->C^{dim1 x dim2}
        H_ij = <i|H|j>
        :param
            bra: ndarray - eigenstates calculated by states()
            ket: ndarray - action of H_t on the eigenstates
            t: float - hopping term
            u: float - on site interaction
            v: float - nearest neighbour interaction
        :return
            mat: ndarray - Hamiltonian represented as matrix
        """
		if self.one_dim:
			dim = 1
			zd = 2.
		else:
			dim = 2
			zd = 4.
		dim1 = np.shape(bra)[0]
		dim2 = np.shape(ket)[0]
		mat = np.zeros((dim1, dim2))
		for i in range(dim1):
			for j in range(i + 1):  # create lower triangular matrix
				for k in ket[j]:
					# t elements
					if np.array_equal(bra[i][1:], k[1:]):
						mat[i, j] += -t * k[0]
				if i == j:
					# constant
					mat[i, j] += self.nsites * (u / 2. + zd * v)
					for l in self.nnlist:  # runs over lattice sites
						# u elements
						if bra[i][l] == 1 and bra[i][l] == bra[i][self.nsites + l]:
							mat[i, j] += u
						# v elements
						for m in range(dim):
							# n_i,up * n_j,up
							if bra[i][l] == 1 and bra[i][l] == bra[i][self.nnlist[l][m]]:
								mat[i, j] += v
							# n_i,down * n_j,down
							if bra[i][self.nsites + l] == 1 and \
								bra[i][self.nsites + l] == bra[i][self.nsites + self.nnlist[l][m]]:
								mat[i, j] += v
							# n_i,up * n_j,down
							if bra[i][l] == 1 and bra[i][l] == bra[i][self.nsites + self.nnlist[l][m]]:
								mat[i, j] += v
							# n_i,down * n_j,up
							if bra[i][self.nsites + l] == 1 and bra[i][self.nsites + l] == bra[i][self.nnlist[l][m]]:
								mat[i, j] += v
						# mu elements
						# n_i,up
						if bra[i][l] == 1:
							mat[i, j] += -mu - (u / 2. + zd * v)
						# n_i,down
						if bra[i][self.nsites + l] == 1:
							mat[i, j] += -mu - (u / 2. + zd * v)
		# Hamiltonian is hermitian : transpose lower triangular matrix, remove diagonal and add up both
		mat += mat.conj().T - np.diag(np.diag(mat))
		return mat

	def matrix_lrc(self, bra, ket, t, u, alpha, mu):
		""" Hamiltonian matrix represented in particle number basis; H:C^{dim1 x dim2}->C^{dim1 x dim2}
		H_ij = <i|H|j>
		:param
			bra: ndarray - eigenstates calculated by states()
			ket: ndarray - action of H_t on the eigenstates
			t: float - hopping parameter
			u: float - on site interaction
			alpha: float - damping parameter
		:return
			mat: ndarray - Hamiltonian represented as matrix
		"""
		dim1 = np.shape(bra)[0]
		#dim2 = np.shape(ket)[0]
		mat = np.zeros((dim1, dim1))
		for i in range(dim1):
			for j in range(i + 1):  # create lower triangular matrix
				for k in ket[j]:
					# t elements
					if np.array_equal(bra[i][1:], k[1:]):
						mat[i, j] += -t * k[0]
				if i == j:
					for l in self.vec_dict:
						# U elements
						if bra[i][l] == 1 and bra[i][l] == bra[i][self.nsites + l]:
							mat[i, j] += u
						# chemical potential and constant of Hubbard interaction
						if bra[i][l] == 1:
							mat[i, j] += -mu - u / 2.
						if bra[i][self.nsites + l] == 1:
							mat[i, j] += -mu - u / 2.
						mat[i, j] += u / 2.
						# V elements
						for k in self.vec_dict:
							if l != k:
								x_min = self.min_dist(self.vec_dict[l], self.vec_dict[k])
								# n_l, up * n_k, up
								if bra[i][l] == 1 and bra[i][l] == bra[i][k]:
									mat[i, j] += self.v_xy(x_min=x_min, alpha=alpha) / 2.
								# n_l,down * n_k,down
								if bra[i][self.nsites + l] == 1 and \
										bra[i][self.nsites + l] == bra[i][self.nsites + k]:
									mat[i, j] += self.v_xy(x_min=x_min, alpha=alpha) / 2.
								# n_l,up * n_k,down
								if bra[i][l] == 1 and bra[i][l] == bra[i][self.nsites + k]:
									mat[i, j] += self.v_xy(x_min=x_min, alpha=alpha) / 2.
								# n_l,down * n_k,up
								if bra[i][self.nsites + l] == 1 and bra[i][self.nsites + l] == bra[i][k]:
									mat[i, j] += self.v_xy(x_min=x_min, alpha=alpha) / 2.
								# chemical potential and constant of extended interactions
								if bra[i][l] == 1:
									mat[i, j] += -mu - self.v_xy(x_min=x_min, alpha=alpha) / 2.
								if bra[i][self.nsites + l] == 1:
									mat[i, j] += -mu - self.v_xy(x_min=x_min, alpha=alpha) / 2.
								if bra[i][k] == 1:
									mat[i, j] += -mu - self.v_xy(x_min=x_min, alpha=alpha) / 2.
								if bra[i][self.nsites + k] == 1:
									mat[i, j] += -mu - self.v_xy(x_min=x_min, alpha=alpha) / 2.
								mat[i, j] += self.v_xy(x_min=x_min, alpha=alpha)
		# Hamiltonian is hermitian : transpose lower triangular matrix, remove diagonal and add up both
		mat += mat.conj().T - np.diag(np.diag(mat))
		return mat


	def plot_matrix(self, mat, t, u, v):
		col = ['tomato', 'snow', 'green', 'darkred', 'navy', 'royalblue']
		cmap = colors.ListedColormap(col)
		d = 0.01
		bounds = [-t - d, -t + d, -d, +d, v - d, v + d, t - d, t + d, u - d, u + d, 2 * u - d, 2 * u + d]
		norm = colors.BoundaryNorm(bounds, cmap.N)
		label = ['-t', '0', 'V', 't', 'U', '2U']
		patches = [mp.Patch(color=col[i], label=str(label[i])) for i in range(len(col))]
		plt.figure('{}x{}_t{}_U{}_V{}'.format(self.nx, self.ny, t, u, v))
		plt.imshow(mat, cmap=cmap, norm=norm)
		plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
		plt.xticks([])
		plt.yticks([])
		# plt.colorbar(cmap=cmap)
		plt.show()
