import numpy as np
import matplotlib.pyplot as plt
import yaml


def get_cfg():
    with open("config.yml", "r") as cfg_file:
        cfg = yaml.load(cfg_file, Loader=yaml.FullLoader)
    return cfg


def diag(n, model, sector):
    # print('Particle sector {} of {} at process: {:11}'.format(n, 2 * model.nsites, 'states'), end='\r')
    if sector == "full":
        states = model.states(n)
    elif sector == "fixN_S0":
        states = model.states_N_S0()  # select number of spin up and spin down electrons; default is equally parted
    else:
        states = []
    # print(states)

    dim = np.shape(states)[0]
    # print('Particle sector {} of {} at process: {:11}'.format(n, 2 * model.nsites, 'tstates'), end='\r')
    tstates = [model.hamt(state=state) for state in states]
    # print(tstates)
    # print('Particle sector {} of {} at process: {:11}'.format(n, 2 * model.nsites,
    #                                                           'matrix '+str(dim)+'x'+str(dim)), end='\r')
    # mat = model.matrix_lrc(states, tstates, t=model.t, u=model.u, alpha=model.v, mu=model.mu)
    mat = model.matrix(states, tstates, t=model.t, u=model.u, v=model.v, mu=model.mu)
    # print(mat)
    # mod.plot_matrix(mat, t, u, v)
    # print('Particle sector {} of {} at process: {:11}'.format(n, 2 * model.nsites,
    #                                                           'diagonalize '+str(dim)+'x'+str(dim)), end='\r')
    w, v = np.linalg.eig(mat)  # computes eigenvalues/ -vectors with geev LAPACK routines
    return w.real, v.real.T, states


def exp_val(T, eigs, mu, exp_value, mat_elem=None):
    """calculate the grand canonical expectation value of an observable whose eigenvalues are known
        (e.g. total particle number or energy)
    :param
        beta: float - inverse temperature (k_B = 1)
        eigs: dict - eigenvalues corresponding to the particle number
        mu:    float - chemical potential
    :return
        val: float - expectation value
    """
    Z = np.sum(np.array([np.sum(np.exp(-1. / T * eigs[n])) * np.exp(1. / T * mu * n) for n in eigs]))
    if exp_value == "part":
        val = np.sum(np.concatenate([n * np.exp(-1. / T * (eigs[n] - mu * n)) for n in eigs])) / Z
    elif exp_value == "docc" or exp_value == "potu" or exp_value == "potv" or exp_value == "kin" or exp_value == "ener"\
            or exp_value == "all" or exp_value == "ener-docc":
        val = np.sum(np.concatenate([mat_elem[n] * np.exp(-1. / T * (eigs[n] - mu * n)) for n in eigs])) / Z
    else:
        val = None
        print("Determine observable for expectation value !")
    return val


def vec2state(vector, states):
    """ express eigenvectors of H in terms of particle number states, i.e. eigenstates of N operator
    :param vector: 2darray - matrix containing the normalized eigenvectors of H
    :param states: 2darray - matrix containing the particle number states
    :return hstates: list - list containing the superposition states
    """
    hstates = []
    for ind, val in enumerate(vector):
        if val:
            states[ind][0] *= val
            hstates.append(states[ind])
    return hstates


def plot(x, y, exp_value, nsites, T=None):
    plt.figure(exp_value)
    plt.plot(x, y, '-', markersize=3., linewidth=0.5)
    if exp_value == "ener" or exp_value == "potu" or exp_value == "potv" or exp_value == "kin":
        plt.ylabel(exp_value)
        plt.xlabel("T")
    elif exp_value == "docc":
        plt.xlabel("T")
        plt.ylabel(u'<n$_{\u2191}$n$_{\u2193}$>')
    elif exp_value == "part":
        plt.title("particle number T=" + T)
        plt.ylabel("<N>")
        plt.xlabel(r"$\mu$")
        # plt.axvline(0, color="k", linestyle="--", linewidth=0.5)
        plt.axhline(nsites, color="k", linestyle="--", linewidth=0.5)
    plt.show()
