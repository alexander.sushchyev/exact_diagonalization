#!/bin/bash

if [ ! -d .venv ]; then
  virtualenv .venv # create virtual env. called '.venv'
  source .venv/bin/activate
  pip3 install -r requirements.txt # install python packages
else
  source .venv/bin/activate
  pip3 list
fi
