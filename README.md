# **E**xact **D**iagonalization #
## General Information ##
This repository contains the python3 code for an exact diagonalization algorithm of the (extended) Hubbard model in particle-hole symmetric formulation on a small 2D square lattice or a 1D lattice chain with periodic boundary conditions. The sums run over the lattice sites i or bonds connecting 2 neighbouring sites <i,j> and the spin.
```math
H = -t\sum_{<i,j>,\sigma}\left( c_{i,\sigma}^{\dagger}c_{j,\sigma} + c_{j,\sigma}^{\dagger}c_{i,\sigma} \right)
+ U\sum_i (n_i -1)^2 + V\sum_{<i,j>} (n_i-1)(n_j-1) + \mu\sum_i n_i
\\
n_i = n_{i,\uparrow} + n_{i,\downarrow}
```
The code is able to calculate the ground state energy and the thermodynamical expectation values of several observables in the grand canonical ensemble, which is the main purpose. For this task the whole Hilbert space is required. Therefore no symmetries that would reduce the computational effort (such as translational symmetry) are considered and implemented.
```math
\left<A\right> = \frac{1}{Z_G}Tr \left( Ae^{-\beta (H-\mu N)} \right)
```
The code has been written and tested (against Quantum Monte Carlo) solely on two Linux distributions, LinuxMint 19.x and OpenSuse 15.x, with a python3_v3.6.9 interpreter.

## Prerequisites ##
It is recommended to use the ED in a virtual environment (venv) so that the python and package versions don't get mixed up. Therefore the following has to be installed:
* Python 3.x
* Python3-pip

Information on virtual environments can be found [here](https://docs.python.org/3/library/venv.html).
To set up the venv run the setup_venv script in the root directory of the code by typing `source ./setup-venv.sh` or `. ./setup_venv.sh` (short for `source`). This script creates the venv called _.venv_ as a hidden directory if it is not created yet and installs automatically the packages listed in `requirements.txt`. If the venv is set up, the script will activate the environment and the the code can be executed. To leave the environment type `deactivate` in the command line.
If you don't want to use a venv, make sure all packages from `requirements.txt` are installed.

## Code structure ##
Additionally to the setup files, the root directory contains the main file that can be executed like a bash script `./my_script`. It invokes the `config.yml` file and the source files in the directory _ed_. The computation involves the following steps:
1. get configuration
2. build electron particle number states |n1,n2,...>, each n counts the electrons on one lattice site
3. calculate the action of the kinetic part on the states
4. represent the Hamiltonian in the particle number basis, H is block diagonal in the particle numbers
5. diagonalize this matrix using `numpy.linalg.eig` that draws on a LAPACK routine for the diagonalization of square matrices
6. calculate the trace (takes more time for the exp.value of the individual parts of the Hamiltonian, since new matrix elements have to be calculated)

## Running the code ##
The user has only to provide the parameters and settings in the `config.yml` file that can be found in the root directoy. 
* `nx`, `ny`: set the number of sites in the respective direction (1D: `ny` = 1).
* `t`, `u`, `v`, `mu`: Hamiltonian parameters, `mu=0` for half filling
* `full_H`: **True**: diagonalize whole Hilbert space blockwise (i.e. all 2N particle subspaces); **False**: diagonalize half filled sector
* `sector`: whole particle sector or reduced to spin S=0 sector
* `exp_value`: choose observable (kinetic energy takes very long, e.g. 4x2 grid >>24h)
* `x_start`, `x_end`, `x_num`: temperature or chemical potential values for a `numpy.linspace(x_start, x_end, x_num)`
* `verbose`: display data

After setting up the config file, type `./main.py` in the command line. Make sure the venv is activated or that all packages are installed. The code displays the current computational task (build states -> apply kin.part to states -> build matrix -> diagonalize (-> calc. trace)) of each particle sector and the size of the corresponding Hilbert space. If one is only interested in the ground state energy, the fastes way to compute it is to reduce the computation to the half filled S=0 sector.
