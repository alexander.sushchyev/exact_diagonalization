#!/usr/bin/env python3

import numpy as np
import timeit
import os
from ed.methods import exp_val, diag, get_cfg, plot, vec2state
from ed.model import Model


def main():
    # float_formatter = "{:.3f}".format
    # np.set_printoptions(formatter={'float_kind': float_formatter}, threshold=np.inf, linewidth=1000)

    cfg = get_cfg()
    nx = cfg['nx']
    ny = cfg['ny']
    if nx > 1 and ny > 1:
        one_dim = False
        zd = 4.
    else:
        one_dim = True
        zd = 2.
    t = cfg['t']
    u = cfg['u']
    v = cfg['v']
    mu = cfg['mu']
    T_part = 0.1  # temperature for particle number expectation value
    full_H = cfg['full_H']
    sector = cfg['sector']
    verbose = cfg['verbose']

    t_start = timeit.default_timer()
    mod = Model(nx, ny, t, u, v, mu, one_dim)
    mod.make_lattice()  # set lattice and calculate number of sites
    mod.get_nnlist()
    mod.get_vec_dict()
    eigs = {}  # store the energy eigenvalues corresponding to the particle number (grand canonical ensemble)
    if full_H:  # diagonalize full Hamiltonian block wise for thermal exp. value <A>(T) = 1/Z*Tr(A*exp(-1/T*(H-mu*N))
        ev = cfg['exp_value']
        x_start = cfg['x_start']
        x_end = cfg['x_end']
        x_num = cfg['x_num']
        eig_vec = {}  # eigenvectors (row wise) of each particle number sector
        x_arr = []
        n_val = []

        if ev == 'ener':
            for n in range(2*mod.nsites+1):
                tn_start = timeit.default_timer()
                eigs[n], eig_vec[n], states = diag(n, mod, sector)
                tn_end = timeit.default_timer()
                print('\nElapsed time sector {0:2d}: {1:6.5f} s'.format(n, tn_end - tn_start))
            x_arr = np.linspace(x_start, x_end, x_num)  # temperature
            n_val = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=eigs) for x in x_arr]
            t_end = timeit.default_timer()
            print('Elapsed time total : {0:6.3f} s'.format(t_end - t_start))

        elif ev == 'part':
            for n in range(2*mod.nsites+1):
                tn_start = timeit.default_timer()
                eigs[n], eig_vec[n], states = diag(n, mod, sector)
                tn_end = timeit.default_timer()
                print('\nElapsed time sector {0:2d}: {1:6.5f} s'.format(n, tn_end - tn_start))
            x_arr = np.linspace(x_start, x_end, x_num)  # chemical potential
            n_val = [exp_val(T=T_part, eigs=eigs, mu=x, exp_value=ev) for x in x_arr]
            t_end = timeit.default_timer()
            print('Elapsed time total : {0:6.3f} s'.format(t_end - t_start))

        elif ev == 'kin':
            mat_elem_kin = {}  # diagonal matrix elements of kinetic part in the basis that diagonalizes the total H
            for n in range(2*mod.nsites+1):
                tn_start = timeit.default_timer()
                eigs[n], eig_vec[n], states = diag(n, mod, sector)
                # express basis states of diagonal H in terms of particle number states
                hstates = [vec2state(vec, np.copy(states)) for vec in eig_vec[n]]
                vals_kin = np.zeros(len(hstates))  # matrix elements for each particle number sector
                dim = np.shape(states)[0]
                print('Particle sector {} of {} at process: {:11}'.format(
                    n, 2 * mod.nsites, 'trace ' + str(dim) + 'x' + str(dim)), end='\r')
                for i in range(len(hstates)):  # tr_n(H_t) (subspace of a distinct particle number)
                    tmp_valt = 0.
                    tstates = [mod.hamt(state=state) for state in hstates[i]]
                    for state in hstates[i]:
                        for tstate in tstates:
                            for vec in tstate:
                                if np.array_equal(state[1:], vec[1:]):
                                    tmp_valt += -t * vec[0] * state[0]
                    vals_kin[i] += tmp_valt
                mat_elem_kin[n] = vals_kin
                tn_end = timeit.default_timer()
                print('\nElapsed time sector {0:2d}: {1:6.5f} s'.format(n, tn_end - tn_start))
            x_arr = np.linspace(x_start, x_end, x_num)  # temperature
            n_val = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=mat_elem_kin) for x in x_arr]
            t_end = timeit.default_timer()
            print('Elapsed time total : {0:6.3f} s'.format(t_end - t_start))

        elif ev == 'potu':
            mat_elem_potu = {}  # diagonal matrix elements of kinetic part in the basis that diagonalizes the total H
            nn = mod.nnlist
            for n in range(2*mod.nsites+1):
                tn_start = timeit.default_timer()
                eigs[n], eig_vec[n], states = diag(n, mod, sector)
                # express basis states of diagonal H in terms of particle number states
                hstates = [vec2state(vec, np.copy(states)) for vec in eig_vec[n]]
                vals_potu = np.zeros(len(hstates))  # matrix elements for each particle number sector
                dim = np.shape(states)[0]
                print('Particle sector {} of {} at process: {:11}'.format(
                    n, 2 * mod.nsites, 'trace ' + str(dim) + 'x' + str(dim)), end='\r')
                for i in range(len(hstates)):  # tr_n(H_U) (subspace of a distinct particle number)
                    tmp_valu = 0.
                    for state in hstates[i]:
                        for l in nn:
                            if state[l] == 1 and state[l] == state[mod.nsites + l]:
                                tmp_valu += state[0] ** 2 * u
                    vals_potu[i] += tmp_valu
                mat_elem_potu[n] = vals_potu
                tn_end = timeit.default_timer()
                print('\nElapsed time sector {0:2d}: {1:6.5f} s'.format(n, tn_end - tn_start))
            x_arr = np.linspace(x_start, x_end, x_num)  # temperature
            n_val = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=mat_elem_potu) for x in x_arr]
            t_end = timeit.default_timer()
            print('Elapsed time total : {0:6.3f} s'.format(t_end - t_start))

        elif ev == 'potv':
            mat_elem_potv = {}  # diagonal matrix elements of kinetic part in the basis that diagonalizes the total H
            dimension = int(zd/2.)
            nn = mod.nnlist
            for n in range(2*mod.nsites+1):
                tn_start = timeit.default_timer()
                eigs[n], eig_vec[n], states = diag(n, mod, sector)
                # express basis states of diagonal H in terms of particle number states
                hstates = [vec2state(vec, np.copy(states)) for vec in eig_vec[n]]
                vals_potv = np.zeros(len(hstates))  # matrix elements for each particle number sector
                dim = np.shape(states)[0]
                print('Particle sector {} of {} at process: {:11}'.format(
                    n, 2 * mod.nsites, 'trace ' + str(dim) + 'x' + str(dim)), end='\r')
                for i in range(len(hstates)):  # tr_n(H_V) (subspace of a distinct particle number)
                    tmp_valv = 0.
                    for state in hstates[i]:
                        for l in nn:
                            for m in range(dimension):
                                # n_i,up * n_j,up
                                if state[l] == 1 and state[l] == state[nn[l][m]]:
                                    tmp_valv += state[0]**2 * v
                                # n_i,down * n_j,down
                                if state[mod.nsites + l] == 1 and \
                                        state[mod.nsites + l] == state[mod.nsites + nn[l][m]]:
                                    tmp_valv += state[0]**2 * v
                                # n_i,up * n_j,down
                                if state[l] == 1 and state[l] == state[mod.nsites + nn[l][m]]:
                                    tmp_valv += state[0]**2 * v
                                # n_i,down * n_j,up
                                if state[mod.nsites + l] == 1 and state[mod.nsites + l] == state[nn[l][m]]:
                                    tmp_valv += state[0]**2 * v
                    vals_potv[i] += tmp_valv
                mat_elem_potv[n] = vals_potv
                tn_end = timeit.default_timer()
                print('\nElapsed time sector {0:2d}: {1:6.5f} s'.format(n, tn_end - tn_start))
            x_arr = np.linspace(x_start, x_end, x_num)  # temperature
            n_val = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=mat_elem_potv) for x in x_arr]
            t_end = timeit.default_timer()
            print('Elapsed time total : {0:6.3f} s'.format(t_end - t_start))

        elif ev == 'docc':
            mat_elem_pot = {}  # diagonal matrix elements of kinetic part in the basis that diagonalizes the total H
            for n in range(2*mod.nsites+1):
                tn_start = timeit.default_timer()
                eigs[n], eig_vec[n], states = diag(n, mod, sector)
                # express basis states of diagonal H in terms of particle number states
                hstates = [vec2state(vec, np.copy(states)) for vec in eig_vec[n]]
                vals_pot = np.zeros(len(hstates))  # matrix elements for each particle number sector
                dim = np.shape(states)[0]
                print('Particle sector {} of {} at process: {:11}'.format(
                    n, 2 * mod.nsites, 'trace ' + str(dim) + 'x' + str(dim)), end='\r')
                for i in range(len(hstates)):  # tr_n(H_int) (subspace of a distinct particle number)
                    tmp_valu = 0.
                    for state in hstates[i]:
                        for l in mod.nnlist:
                            if state[l] == 1 and state[l] == state[mod.nsites + l]:
                                tmp_valu += state[0] ** 2 * u
                    vals_pot[i] += tmp_valu
                mat_elem_pot[n] = vals_pot
                tn_end = timeit.default_timer()
                print('\nElapsed time sector {0:2d}: {1:6.5f} s'.format(n, tn_end - tn_start))
            x_arr = np.linspace(x_start, x_end, x_num)  # temperature
            n_val = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=mat_elem_pot)/(u*mod.nsites) for x in x_arr]
            t_end = timeit.default_timer()
            print('Elapsed time total : {0:6.3f} s'.format(t_end - t_start))

        elif ev == 'all':
            mat_elem_potv = {}  # diagonal matrix elements of kinetic part in the basis that diagonalizes the total H
            mat_elem_kin = {}
            mat_elem_potu = {}
            dimension = int(zd/2.)
            nn = mod.nnlist
            for n in range(2*mod.nsites+1):
                tn_start = timeit.default_timer()
                eigs[n], eig_vec[n], states = diag(n, mod, sector)
                # express basis states of diagonal H in terms of particle number states
                hstates = [vec2state(vec, np.copy(states)) for vec in eig_vec[n]]
                vals_potv = np.zeros(len(hstates))  # matrix elements for each particle number sector
                vals_kin = np.zeros(len(hstates))
                vals_potu = np.zeros(len(hstates))
                dim = np.shape(states)[0]
                print('Particle sector {} of {} at process: {:11}'.format(
                    n, 2 * mod.nsites, 'trace ' + str(dim) + 'x' + str(dim)), end='\r')
                for i in range(len(hstates)):  # tr_n(H_V) (subspace of a distinct particle number)
                    tmp_valv = 0.
                    tmp_valt = 0.
                    tmp_valu = 0.
                    tstates = [mod.hamt(state=state) for state in hstates[i]]
                    for state in hstates[i]:
                        for tstate in tstates:
                            for vec in tstate:
                                if np.array_equal(state[1:], vec[1:]):
                                    tmp_valt += -t * vec[0] * state[0]
                        for l in nn:
                            if state[l] == 1 and state[l] == state[mod.nsites + l]:
                                tmp_valu += state[0] ** 2 * u
                            for m in range(dimension):
                                # n_i,up * n_j,up
                                if state[l] == 1 and state[l] == state[nn[l][m]]:
                                    tmp_valv += state[0]**2 * v
                                # n_i,down * n_j,down
                                if state[mod.nsites + l] == 1 and \
                                        state[mod.nsites + l] == state[mod.nsites + nn[l][m]]:
                                    tmp_valv += state[0]**2 * v
                                # n_i,up * n_j,down
                                if state[l] == 1 and state[l] == state[mod.nsites + nn[l][m]]:
                                    tmp_valv += state[0]**2 * v
                                # n_i,down * n_j,up
                                if state[mod.nsites + l] == 1 and state[mod.nsites + l] == state[nn[l][m]]:
                                    tmp_valv += state[0]**2 * v
                    vals_potv[i] += tmp_valv
                    vals_kin[i] += tmp_valt
                    vals_potu[i] += tmp_valu
                mat_elem_potu[n] = vals_potu
                mat_elem_potv[n] = vals_potv
                mat_elem_kin[n] = vals_kin
                tn_end = timeit.default_timer()
                print('\nElapsed time sector {0:2d}: {1:6.5f} s'.format(n, tn_end - tn_start))
            x_arr = np.linspace(x_start, x_end, x_num)  # temperature
            n_val_ener = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=eigs) for x in x_arr]
            n_val_potv = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=mat_elem_potv) for x in x_arr]
            n_val_kin = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=mat_elem_kin) for x in x_arr]
            n_val_potu = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=mat_elem_potu) for x in x_arr]
            n_val_docc = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=mat_elem_potu) /
                          (u * mod.nsites) for x in x_arr]
            n_val = {'ener': n_val_ener, 'kin': n_val_kin, 'potu': n_val_potu, 'potv': n_val_potv, 'docc': n_val_docc}
            t_end = timeit.default_timer()
            print('Elapsed time total : {0:6.3f} s'.format(t_end - t_start))

        elif ev == 'ener-docc':
            mat_elem_potu = {}
            nn = mod.nnlist
            for n in range(2*mod.nsites+1):
                tn_start = timeit.default_timer()
                eigs[n], eig_vec[n], states = diag(n, mod, sector)
                # express basis states of diagonal H in terms of particle number states
                hstates = [vec2state(vec, np.copy(states)) for vec in eig_vec[n]]
                vals_potu = np.zeros(len(hstates))
                dim = np.shape(states)[0]
                print('Particle sector {} of {} at process: {:11}'.format(
                    n, 2 * mod.nsites, 'trace ' + str(dim) + 'x' + str(dim)), end='\r')
                for i in range(len(hstates)):  # tr_n(H_V) (subspace of a distinct particle number)
                    tmp_valu = 0.
                    for state in hstates[i]:
                        for l in nn:
                            if state[l] == 1 and state[l] == state[mod.nsites + l]:
                                tmp_valu += state[0] ** 2 * u
                    vals_potu[i] += tmp_valu
                mat_elem_potu[n] = vals_potu
                tn_end = timeit.default_timer()
                print('\nElapsed time sector {0:2d}: {1:6.5f} s'.format(n, tn_end - tn_start))
            x_arr = np.linspace(x_start, x_end, x_num)  # temperature
            n_val_ener = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=eigs) for x in x_arr]
            n_val_docc = [exp_val(T=x, eigs=eigs, mu=mu, exp_value=ev, mat_elem=mat_elem_potu) /
                          (u * mod.nsites) for x in x_arr]
            n_val = {'ener': n_val_ener, 'docc': n_val_docc}
            t_end = timeit.default_timer()
            print('Elapsed time total : {0:6.3f} s'.format(t_end - t_start))

        # print options
        if verbose == 1:
            print()
            print('Ground state energy (@half filling): ', min(eigs[mod.nsites]))
            if ev == 'all' or ev == 'ener-docc':
                print()
                print('temperature')
                print(list(x_arr))
                for val in n_val:
                    print(val)
                    print(n_val[val])
            elif ev == 'part':
                print()
                print('mu')
                print((list(x_arr)))
                print(ev, ' T=', T_part)
                print(n_val)
            else:
                print()
                print('temperature')
                print(list(x_arr))
                print(ev)
                print(n_val)
        elif verbose == 2:
            print()
            print('Ground state energy (@half filling): ', min(eigs[mod.nsites]))
            if ev == 'all' or ev == 'ener-docc':
                for val in n_val:
                    plot(x_arr, n_val[val], exp_value=val, nsites=mod.nsites)
            elif ev == 'part':
                plot(x_arr, n_val, ev, mod.nsites, T=str(T_part))
            else:
                plot(x_arr, n_val, ev, mod.nsites)
        elif verbose == 3:
            if not os.path.exists('results'):
                os.makedirs('results')
            output = str(nx)+'x'+str(ny)+'_T'+str(x_start)+'-'+str(x_end)+'_t'+str(t)+'_u'+str(u)+'_v'+str(v)+'_'+ev
            width = '{:^17}'
            with open('results/' + output, 'w') as outfile:
                outfile.write('Ground state energy (@half filling): {}\n\n'.format(min(eigs[mod.nsites])))
                if ev == 'all' or ev == 'ener-docc':
                    header = width.format('T')
                    for val in n_val:
                        header += '\t'+width.format(val)
                    outfile.write(header+'\n')
                    for i in range(len(x_arr)):
                        line = width.format(x_arr[i])
                        for val in n_val:
                            line += '\t'+width.format(n_val[val][i])
                        outfile.write(line+'\n')
                else:
                    width = '{:^17}\t{:^17}'
                    if ev == 'part':
                        header = width.format('mu', ev)
                    else:
                        header = width.format('T', ev)
                    outfile.write(header + '\n')
                    for i in range(len(x_arr)):
                        line = width.format(x_arr[i], n_val[i])
                        outfile.write(line + '\n')
            outfile.close()
        else:
            pass

    else:  # if not full Hamiltonian, restrict diagonalization to half filled part of Hamiltonian with S=0
        ev = cfg['exp_value']
        if ev == 'test':
            n = 6
            eigs[mod.nsites] = diag(n, mod, sector)[0]
            for i in eigs[mod.nsites]:
                print('{:2.1f}'.format(i))
        else:
            n = mod.nsites
            eigs[n] = diag(n, mod, sector)[0]
            t_end = timeit.default_timer()
            # print('\nElapsed time : {:6.3f}s'.format(t_end - t_start))
            print('Ground state energy (@half filling): ', min(eigs[n]))


if __name__ == '__main__':
    np.set_printoptions(linewidth=150, threshold=np.inf)
    # np.warnings.filterwarnings('error', category=np.VisibleDeprecationWarning)
    main()
